# First Assignment

## Name
Jack Hagedorn Jensen

## Technologies
* Java 17
* JUnit 5

## Description
Project is made out from the assignment given from Noroff LMS. 
<br> The project focuses on class handling and general project structure.
<br> It also focuses inheritance and testing of said classes.

## Project status
I didn't complete the entire project as expected. 
This is due to in my opinion bad structure of the assignment explanation. 
There are some criteria that aren't explained until the last slide, 
which should've been pointed out in the equipment section.
<br> Therefore, I haven't added the proper attributes to the armour class, 
and I haven't completed the testing. I know this will affect my grade, 
and I will just look forward to the feedback and see what I could've done better.