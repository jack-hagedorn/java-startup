package test;

import character.characterClass.Mage;
import equipment.Armour;
import exceptions.InvalidArmourException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class InvalidArmourExceptionTest {
    Mage testMage = new Mage("Test");
    Armour testArmour = new Armour(
            "Test",
            2,
            "Head",
            "Armour",
            5,
            "Cloth"
    );

    @Disabled
    @Test
    public void expectedTextTest() throws InvalidArmourException {

    }
}