package test;


import character.characterClass.Rogue;
import character.characterClass.Warrior;
import equipment.Armour;
import equipment.Equipment;
import equipment.Weapon;
import exceptions.InvalidArmourException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class CharacterTest {
    static Warrior testWarrior = new Warrior("Test");
    Armour headSlot = new Armour(
            "Helm",
            1,
            "Head",
            "Armour",
            1,
            "Mail");

    Armour bodySlot = new Armour(
            "Body",
            1,
            "Body",
            "Armour",
            1,
            "Mail"
    );

    Armour legSlot = new Armour(
            "Legs",
            1,
            "Legs",
            "Armour",
            1,
            "Mail"
    );

    Weapon weaponSlot = new Weapon(
            "Weapon",
            1,
            "Weapon",
            5,
            "Axe",
            5
    );

    @Test
    void getDpsTest() {
        double expected = 1 + (double) testWarrior.getStrength() / 100;

        double actual = testWarrior.getDps();

        assert actual == expected;
    }

    @Test
    void getPrimaryAttributeTotalTest() {
        int expected = 5;

        int actual = testWarrior.getPrimaryAttributeTotal();

        assert expected == actual;
    }

    @Test
    void getArmourTest() throws InvalidArmourException {
        testWarrior.addArmour(legSlot);
        testWarrior.addArmour(bodySlot);
        testWarrior.addArmour(headSlot);

        Map<String, Armour> expected = new HashMap<>();
        expected.put("Legs",legSlot);
        expected.put("Body", bodySlot);
        expected.put("Head", headSlot);

        assert expected.equals(testWarrior.getArmours());
    }
}