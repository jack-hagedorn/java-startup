package equipment;

public class Weapon extends Equipment {

    private final double damage;
    private final double attackSpeed;

    // Weapon constructor.
    public Weapon(String name, int requiredLevel, String equipmentSlot, double damage, String type, double attackSpeed) {
        super(name, requiredLevel, equipmentSlot, type);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    // Getters.
    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Name= " + name;
    }
}
