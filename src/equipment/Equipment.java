package equipment;

public abstract class Equipment {

    protected String name;
    protected int requiredLevel;
    protected String equipmentSlot;
    protected String type;
    protected int primaryAttribute;

    public Equipment(String name, int requiredLevel, String equipmentSlot, String type) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.equipmentSlot = equipmentSlot;
        this.type = type;
    }

    // Getters
    public int getPrimaryAttribute() {
        return primaryAttribute;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public String getEquipmentSlot() {
        return equipmentSlot;
    }

    public String getType() {
        return type;
    }

}
