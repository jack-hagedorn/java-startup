package equipment;

public class Armour extends Equipment {

    private final String material;

    // Armour constructor.
    public Armour(String name, int requiredLevel, String equipmentSlot, String type, int primaryAttribute, String material) {
        super(name, requiredLevel, equipmentSlot, type);
        this.primaryAttribute = primaryAttribute;
        this.material = material;
    }

    public int getPrimaryAttribute() {
        return primaryAttribute;
    }

    public String getMaterial() {
        return material;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Name= " + name;
    }
}
