package character;

import character.characterClass.Attributes;
import equipment.Armour;
import equipment.Equipment;
import equipment.Weapon;
import exceptions.InvalidArmourException;
import exceptions.InvalidWeaponException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Character implements Attributes {

    private String name;
    protected int level;
    protected Map<String, Equipment> equipmentMap;
    protected String[] usableWeapons;
    protected String[] usableArmour;
    protected String primaryAttribute;
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    // Super character constructor.
    public Character(String name) {
        this.name = name;
        this.level = 1;
        this.equipmentMap = new HashMap<>();
        populateEquipmentMap();
    }

    // Populate equipment map.
    public void populateEquipmentMap() {
        equipmentMap.put("Head", null);
        equipmentMap.put("Body", null);
        equipmentMap.put("Legs", null);
        equipmentMap.put("Weapon", null);
    }

    // Getting print of equipped items.
    public void getEquipment() {
        equipmentMap.forEach((key, value) -> {
                    if (value == null) {
                        System.out.println("You currently don't have anything equipped in the " + key + " slot");
                    } else {
                        System.out.println("You're equipping a: " + value
                                + " in your: "
                                + key + " slot");
                    }
                }
        );
    }

    // Getting the total dps value.
    public double getDps() {
        if (getWeapon() == null) {
            return 1 + (double) getPrimaryAttributeTotal() / 100;
        } else {
            return getWeapon().getAttackSpeed() * (1 + (double) getPrimaryAttributeTotal() / 100);
        }
    }

    // Calculates and returns the primary attribute total.
    public int getPrimaryAttributeTotal() {
        AtomicInteger returnValue = new AtomicInteger();

        switch (primaryAttribute) {
            case "Intelligence" -> returnValue.addAndGet(intelligence);
            case "Strength" -> returnValue.addAndGet(strength);
            case "Dexterity" -> returnValue.addAndGet(dexterity);
        }

        getArmours().forEach((key, value) -> {
            if (value != null) {
                returnValue.addAndGet(value.getPrimaryAttribute());
            }
        });
        return returnValue.get();
    }

    // Gets all equipped armours so that primary attribute can be calculated.
    public Map<String, Armour> getArmours() {
        Map<String, Armour> returnList = new HashMap<>();

        equipmentMap.forEach((key, value) -> {
            if (value != null && value.getType().equals("Armour")) {
                returnList.put(key, (Armour) value);
            }
        });

        return returnList;
    }

    // Gets the weapon so that DPS can be calculated.
    public Weapon getWeapon() {
        if (equipmentMap.get("Weapon") == null) {
            return null;
        } else {
            return (Weapon) equipmentMap.get("Weapon");
        }
    }

    // Adds armour to the Equipment Hashmap.
    public void addArmour(Armour armour) throws InvalidArmourException {
        if (armour.getRequiredLevel() > level) {
            throw new InvalidArmourException("You're not high enough level for this armour");
        } else {
            for (String item : getUsableArmour()) {
                if (Objects.equals(item, armour.getMaterial())) {
                    equipmentMap.put(armour.getEquipmentSlot(), armour);
                }
            }
        }
    }

    // Adds weapon to the Equipment Hashmap.
    public void addWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getRequiredLevel() > level) {
            throw new InvalidWeaponException("You're not high enough level for this weapon");
        } else {
            for (String item : getUsableWeapons()) {
                if (Objects.equals(item, weapon.getType())) {
                    equipmentMap.put(weapon.getEquipmentSlot(), weapon);
                }
            }
        }
    }

    // Getters.
    public String[] getUsableWeapons() {
        return usableWeapons;
    }

    public String[] getUsableArmour() {
        return usableArmour;
    }

    public int getLevel() {
        return level;
    }

    public String getPrimaryAttribute() {
        return primaryAttribute;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }


    // To string method.
    @Override
    public String toString() {
        return "Name: " + name
                + "\nLevel: " + level
                + "\nStrength: " + strength
                + "\nIntelligence: " + intelligence
                + "\nDexterity: " + dexterity
                + "\nDPS: " + getDps();
    }
}
