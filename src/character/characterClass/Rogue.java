package character.characterClass;

import character.Character;
import equipment.Weapon;

public class Rogue extends Character {

    // Rogue constructor.
    public Rogue(String name) {
        super(name);
        this.strength = 2;
        this.dexterity = 6;
        this.intelligence = 1;
        this.usableArmour = new String[]{"Leather", "Mail"};
        this.usableWeapons = new String[]{"Dagger", "Sword"};
        this.primaryAttribute = "Dexterity";
    }

    @Override
    public void levelUp() {
        this.level += 1;
        this.strength += 1;
        this.dexterity += 4;
        this.intelligence += 1;
    }

    @Override
    public double dealDamage() {
        Weapon weapon = (Weapon) getWeapon();
        if (weapon == null) {
            return dexterity;
        } else {
            return (dexterity + weapon.getDamage()) * weapon.getAttackSpeed();
        }
    }

}
