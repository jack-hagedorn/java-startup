package character.characterClass;

import character.Character;
import equipment.Weapon;

public class Mage extends Character {

    // Mage constructor.
    public Mage(String name) {
        super(name);
        this.strength = 1;
        this.dexterity = 1;
        this.intelligence = 8;
        this.usableWeapons = new String[]{"Wand", "Staff"};
        this.usableArmour = new String[]{"Cloth"};
        this.primaryAttribute = "Intelligence";
    }

    @Override
    public void levelUp() {
        this.level += 1;
        this.strength += 1;
        this.dexterity += 1;
        this.intelligence += 5;
    }

    @Override
    public double dealDamage() {
        Weapon weapon = (Weapon) getWeapon();
        if (weapon == null) {
            return intelligence;
        } else {
            return (intelligence + weapon.getDamage()) * weapon.getAttackSpeed();
        }
    }


}
