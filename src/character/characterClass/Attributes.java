package character.characterClass;

public interface Attributes {

    // Level up method for adding new attributes.
    public void levelUp();

    // Damage method to calculate total damage output.
    public double dealDamage();
}
