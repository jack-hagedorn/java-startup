package character.characterClass;

import character.Character;
import equipment.Weapon;

public class Warrior extends Character {

    // Warrior constructor.
    public Warrior(String name) {
        super(name);
        this.strength = 5;
        this.dexterity = 2;
        this.intelligence = 1;
        this.usableArmour = new String[]{"Mail"};
        this.usableWeapons = new String[]{"Axe", "Hammer", "Sword"};
        this.primaryAttribute = "Strength";
    }

    @Override
    public void levelUp() {
        this.level += 1;
        this.strength += 3;
        this.dexterity += 2;
        this.intelligence += 1;
    }

    @Override
    public double dealDamage() {
        Weapon weapon = (Weapon) getWeapon();
        if (weapon == null) {
            return strength;
        } else {
            return (strength + weapon.getDamage()) * weapon.getAttackSpeed();
        }
    }
}
