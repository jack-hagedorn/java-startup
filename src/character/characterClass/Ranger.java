package character.characterClass;

import character.Character;
import equipment.Weapon;

public class Ranger extends Character {

    // Ranger constructor.
    public Ranger(String name) {
        super(name);
        this.strength = 1;
        this.dexterity = 7;
        this.intelligence = 1;
        this.usableArmour = new String[]{"Leather", "Mail"};
        this.usableWeapons = new String[]{"Bow"};
        this.primaryAttribute = "Dexterity";
    }

    @Override
    public void levelUp() {
        this.level += 1;
        this.strength += 1;
        this.dexterity += 5;
        this.intelligence += 1;
    }

    @Override
    public double dealDamage() {
        Weapon weapon = (Weapon) getWeapon();
        if (weapon == null){
            return dexterity;
        } else {
            return (dexterity + weapon.getDamage()) * weapon.getAttackSpeed();
        }
    }

}
