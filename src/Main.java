import character.characterClass.Mage;
import equipment.Armour;
import equipment.Weapon;
import exceptions.InvalidArmourException;
import exceptions.InvalidWeaponException;

public class Main {

    public static void main(String[] args) throws InvalidWeaponException, InvalidArmourException {

        Mage myMage = new Mage("Jack");

        myMage.levelUp();

        Weapon staff = new Weapon(
                "Staff of Intelligence",
                1,
                "Weapon",
                5,
                "Staff",
                5
        );

        Armour helmet = new Armour(
                "Helmet of Vast Knowledge",
                1,
                "Head",
                "Armour",
                10,
                "Cloth"
        );

        myMage.addWeapon(staff);
        myMage.addArmour(helmet);

        System.out.println(myMage);
    }
}
